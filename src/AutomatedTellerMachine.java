import java.util.Scanner;

public class AutomatedTellerMachine {
    Scanner scanner = new Scanner(System.in);
    float currentBalance = 20_000F;

    public void showMenu() {
        while (true) {
            System.out.println("(1) Balance Inquiry");
            System.out.println("(2) Withdraw");
            System.out.println("(3) Deposit");
            System.out.println("(4) Exit");
            System.out.print("(*) Enter choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    displayCurrentBalance();
                    break;
                case 2:
                    withdraw();
                    break;
                case 3:
                    deposit();
                    break;
                case 4:
                    exit();
                    break;
                default:
                    System.out.println("Invalid choice.");
            }
            System.out.println("\n-----------------\n");
        }
    }

    public void displayCurrentBalance() {
        System.out.println("Current Balance: " + currentBalance);
    }

    public void deposit() {
        System.out.print("Enter Amount: ");
        float depositAmount = scanner.nextFloat();
        if (depositAmount > 0) {
            currentBalance += depositAmount;
            System.out.println("Deposit Transaction Successful.");
        } else {
            System.out.println("Invalid amount.");
        }
    }

    public void withdraw() {
        System.out.print("Enter Amount: ");
        float withdrawAmount = scanner.nextFloat();
        if ((withdrawAmount % 100) == 0 && withdrawAmount <= 2000 && withdrawAmount <= currentBalance) {
            currentBalance -= withdrawAmount;
            System.out.println("Withdraw Transaciton Successful.");
        } else {
            System.out.println("Invalid amount.");
        }
    }

    public void exit() {
        System.exit(0);
    }

    public static void main(String[] args) {
        AutomatedTellerMachine atm = new AutomatedTellerMachine();
        atm.showMenu();
    }
}
