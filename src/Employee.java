import java.util.Scanner;

public class Employee {
    String firstName;
    String lastName;
    int age;
    float salary;

    public void getEmployeeInformation() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter First Name: ");
        firstName = scanner.nextLine();

        System.out.print("Enter Last Name: ");
        lastName = scanner.nextLine();

        System.out.print("Enter Age: ");
        age = scanner.nextInt();

        System.out.print("Enter Salary: ");
        salary = scanner.nextFloat();
    }

    public void displayEmployeeInformation() {
        System.out.println("Full Name: " + lastName + ", " + firstName);
        System.out.println("Age: " + age);
        System.out.println("Salary: PHP " + salary);
    }

    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.getEmployeeInformation();
        employee.displayEmployeeInformation();

    }
}
