import java.util.Scanner;

public class AgeValidator {
    public boolean isAgeValid(int age) {
        if (age >= 18 && age <= 60) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Runtime.getRuntime().gc();
        AgeValidator ageValidator = new AgeValidator();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter age: ");
        int age = scanner.nextInt();

        if (ageValidator.isAgeValid(age)) {
            System.out.println("Valid age.");
        } else {
            System.out.println("Invalid age.");
        }

    }
}
