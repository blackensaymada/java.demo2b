
public class Calculator {

    public int add(int num1, int num2) {
        int sum = num1 + num2;
        return sum;
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int result = calculator.add(1, 5); 
        System.out.println("Result: " + result);
    }

}
